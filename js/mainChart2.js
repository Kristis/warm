//Drop list
$(document).ready(function(){
	drawChartSunkus1();
	drawChartSunkus2();

	drawChartSunkusPie1();
	drawChartSunkusPie2();
	drawChartSunkusPie3();



});
$(".dropdown-trigger").dropdown();

//Sunkus pie chart1
function drawChartSunkusPie1() {
	var ctx = document.getElementById("myChartPie1");
      var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
          labels: ["Wins", "Loses"],
          datasets: [{
            label: '# of Votes',
            data: [88, 17],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
      },
    });
 };

 //Sunkus pie chart2
function drawChartSunkusPie2() {
	var ctx = document.getElementById("myChartPie2");
      var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
          labels: ["Day", "Night"],
          datasets: [{
            label: '# of Votes',
            data: [74, 26],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
      },
    });
 };

 //Sunkus pie chart3
function drawChartSunkusPie3() {
	var ctx = document.getElementById("myChartPie3");
      var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
          labels: ["Dota 255", "Fortnite", "Worms", "CS", "Snake", "PUBG"],
          datasets: [{
            label: '# of Votes',
            data: [18, 12, 8, 5, 44, 11],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
      },
    });
 };

//Sunkus bar chart1
function drawChartSunkus1() {
	var ctx = document.getElementById("myChart1");
      var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ["Reaction2", "Defend", "Aggression", "Emotions", "Accuracy", "Trash talk"],
          datasets: [{
            label: 'Counter',
            data: [55, 6, 13, 22, 12, 11],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
      },
      options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
          }
      }
    });
 };

//Sunkus bar chart2
function drawChartSunkus2() {
      var ctx = document.getElementById("myChart2");
      var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ["Reaction", "Defend", "Aggression", "Emotions", "Accuracy", "Trash talk"],
          datasets: [{
            label: 'Counter',
            data: [22, 9, 35, 15, 22, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
      },
      options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
          }
      }
    });
};


