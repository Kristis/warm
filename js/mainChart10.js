//Drop list
$(document).ready(function(){
	drawChartSunkus1();
	drawChartSunkus2();

	drawChartSunkusPie1();
	drawChartSunkusPie2();
	drawChartSunkusPie3();



});
$(".dropdown-trigger").dropdown();

//Sunkus pie chart1
function drawChartSunkusPie1() {
	var ctx = document.getElementById("myChartPie1");
      var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
          labels: ["Wins", "Loses"],
          datasets: [{
            label: '# of Votes',
            data: [67, 33],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
      },
    });
 };

 //Sunkus pie chart2
function drawChartSunkusPie2() {
	var ctx = document.getElementById("myChartPie2");
      var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
          labels: ["Day", "Night"],
          datasets: [{
            label: '# of Votes',
            data: [20, 80],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
      },
    });
 };

 //Sunkus pie chart3
function drawChartSunkusPie3() {
	var ctx = document.getElementById("myChartPie3");
      var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
          labels: ["Dota 2", "CrashJuice", "Worms", "Snake", "Chess", "PUBG"],
          datasets: [{
            label: '# of Votes',
            data: [2, 5, 6, 55, 75, 66],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
      },
    });
 };

//Sunkus bar chart1
function drawChartSunkus1() {
	var ctx = document.getElementById("myChart1");
      var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ["Reaction", "Defend", "Aggression", "Emotions", "Accuracy", "Trash talk"],
          datasets: [{
            label: 'Counter',
            data: [55, 85, 68, 2, 6, 2],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
      },
      options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
          }
      }
    });
 };

//Sunkus bar chart2
function drawChartSunkus2() {
      var ctx = document.getElementById("myChart2");
      var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ["Reaction", "Defend", "Aggression", "Emotions", "Accuracy", "Trash talk"],
          datasets: [{
            label: 'Counter',
            data: [72, 82, 88, 9, 47, 77],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
      },
      options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
          }
      }
    });
};


